<?php get_header(); ?>
	<section class="content-section">
		<div class="container">
			<div id="content">
				<?php get_template_part( 'blocks/not_found' ); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</section>
<?php get_footer(); ?>