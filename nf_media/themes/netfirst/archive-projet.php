<?php get_header(); ?>
	<a href="#" class="scroll-down"><img src="<?php echo get_template_directory_uri() ?>/fonts/scroll.svg" alt="image description"><span><?php _e( 'Devouvrir', 'netfirst' ) ?></span></a>
	
	<div class="line-1"></div>
	<div class="line-2"></div>
	<div class="line-3"></div>
	<div class="line-4"></div>
	<div class="line-5"></div>
	
	<section class="project-section">
		<div class="secondhead-holder">
			<?php $post_type_obj = get_post_type_object( 'projet' ); ?>
			<h1><?php echo $post_type_obj->labels->name ?></h1>
		</div>
		<div class="container">
			<?php $terms = get_terms( 'projet-filter' );
			if( $terms ) : ?>
				<ul id="filter">
					<?php foreach( $terms as $term ) :
						$icon = get_field( 'project_filter_icon', $term );
						$title_for_filter = get_field( 'project_filter_rewrite_title_for_filter', $term );?>
						<li><a href="<?php echo get_term_link( $term ) ?>" data-filter="<?php echo $term->slug ?>"><?php echo $title_for_filter ? $title_for_filter : $term->name ?><?php if( $icon ) : ?><div class="img-holder"><img src="<?php echo $icon['url'] ?>" alt="<?php echo $icon['alt'] ?>"></div><?php endif ?></a></li>
					<?php endforeach ?>
				</ul>
			<?php endif ?>
			
			<?php if ( have_posts() ) : ?>
				<div class="items-holder">
					<?php while ( have_posts() ) : the_post();
						$post_terms = wp_get_post_terms( get_the_ID(), 'projet-filter' );
						$custom_excerpt = get_field('custom_excerpt') ;
						$filter = array();
						if( $post_terms ) {
							foreach( $post_terms as $post_term ){
								$filter[] = $post_term->slug;
							}
						} ?>
						<a href="<?php the_permalink() ?>" data-hover="hover-on-link" data-filter-value="<?php echo implode( ' ', $filter ) ?>">
							<div class="text-holder">
								<h1><?php the_title() ?></h1>
								<?php  

									if( $custom_excerpt ){
										echo $custom_excerpt ;
									}

								?>
							</div>
							<div class="img-holder">
								<?php $screen_image = get_field( 'screen_image' );
								if( $screen_image ) : ?>
									<img src="<?php echo $screen_image['url'] ?>" alt="<?php echo $screen_image['alt'] ?>">
								<?php endif ?>
							</div>
						</a>
					<?php endwhile; ?>
					<?php get_template_part( 'blocks/pager' ); ?>
				</div>
			<?php else : ?>
				<?php get_template_part( 'blocks/not_found' ); ?>
			<?php endif; ?>
		</div>
	</section>

	<?php get_template_part( 'blocks/before_footer' ) ?>
	
<?php get_footer(); ?>