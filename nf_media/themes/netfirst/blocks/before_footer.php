<?php

	$home_page_id = absint( get_option( 'page_on_front' ) );

	$contact_section_image = get_field('contact_section_image', $home_page_id) ;
	$contact_section_subtitle = get_field('contact_section_subtitle', $home_page_id) ;
	$contact_form_shortcode = get_field('contact_form_shortcode', $home_page_id) ;
	$contact_form_text = get_field('contact_form_text', $home_page_id) ;

?>

<?php if( $contact_section_subtitle || $contact_form_shortcode ) : ?>
	<section id="contact-form-block" class="contact-section" <?php if( $contact_section_image ) : ?> style="background-image: url(<?php echo $contact_section_image ?>);" <?php endif ; ?> >
		<div class="container">
			<div class="head-section">
				
				<?php if( $contact_section_subtitle ) : ?>
					<h2><?php echo $contact_section_subtitle ; ?></h2>
				<?php endif ; ?>

			</div>

			<?php if( $contact_form_shortcode ) : ?>
				<div class="form-holder">

					<?php echo do_shortcode($contact_form_shortcode) ; ?>
					
					<?php if( $contact_form_text ) : ?>
						<div class="form-info">
							<p><?php echo $contact_form_text ; ?></p>
						</div>
					<?php endif ; ?>
					
				</div>
			<?php endif ; ?>
		</div>
	</section>
<?php endif ; ?>