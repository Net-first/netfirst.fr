<?php
	the_posts_pagination( array(
		'prev_text' => __( 'Previous page', 'netfirst' ),
		'next_text' => __( 'Next page', 'netfirst' ),
	) );
?>
