			</main>

			<?php  

				$footer_logo = get_field('footer_logo','option') ;
				$footer_address_title = get_field('footer_address_title','option') ;
				$footer_address = get_field('footer_address','option') ;
				$footer_email = get_field('footer_email','option') ;
				$footer_phone = get_field('footer_phone','option') ;

			?>

			<footer class="footer">

				<?php if( $footer_logo ) : ?>
					<div class="logo-holder">
						<a href="<?php echo home_url(); ?>" class="logo">
							<img src="<?php echo $footer_logo['url'] ; ?>" alt="<?php echo $footer_logo['alt'] ; ?>">
						</a>
					</div>
				<?php endif ; ?>
				
				<?php if( $footer_address_title || $footer_address ) : ?>
					<ul class="adress-list">

						<?php if( $footer_address_title ) : ?>
							<li><?php echo $footer_address_title ?></li>
						<?php endif ; ?>

						<?php if( $footer_address ) : ?>
							<li>
								<address><?php echo $footer_address ; ?></address> 
							</li>
						<?php endif ; ?>

					</ul>
				<?php endif ; ?>
				
				<?php if( $footer_email || $footer_phone ) : ?>
					<ul class="contact-list">

						<?php if( $footer_email ) : ?>
							<li>
								<a href="mailto:<?php echo antispambot($footer_email) ; ?>"><?php echo antispambot($footer_email) ; ?></a>
							</li>
						<?php endif ; ?>
						
						<?php if( $footer_phone ) : ?>
							<li>
								<a href="tel:<?php echo clean_phone($footer_phone) ; ?>"><?php echo $footer_phone ; ?></a>
							</li>
						<?php endif ; ?>

					</ul>
				<?php endif ; ?>

				<?php if( have_rows('social_networks','option') ): ?>

					<ul class="icon-list">

					    <?php while ( have_rows('social_networks','option') ) : the_row(); ?>

					        <li>
								<a href="<?php echo get_sub_field('url') ; ?>" class="icon-<?php echo get_sub_field('icon') ; ?>" target="blank"></a>
							</li>

					    <?php endwhile; ?>

					</ul>

				<?php endif; ?>

			</footer>
		</div>
		<?php wp_footer(); ?>
	</body>
</html>