<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta charset="<?php bloginfo( 'charset' ); ?>">	
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-40283663-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-40283663-1');
		</script>

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<div id="wrapper" class="wrapper">
	
			<?php  

				$header_logo = get_field('header_logo','option') ;

			?>
			
			<header class="header">
				<div class="container">

					<?php if( $header_logo ) : ?>
						<a href="<?php echo home_url(); ?>">
							<img src="<?php echo $header_logo['url'] ; ?>" alt="<?php echo $header_logo['alt'] ; ?>">
						</a>
					<?php endif ; ?>
					
					<nav class="menu">
					<a href="#" class="nav-opener">
						<span></span>
					</a>
					<div class="drop">
						<div class="line-1"></div>
						<div class="line-2"></div>
						<div class="line-3"></div>
						<div class="line-4"></div>
						<div class="line-5"></div>
						<div class="container">
							<div class="header-info">
								<h2><?php _e( 'Menu', 'netfirst' ) ?></h2>
								
								<?php $footer_address_title = get_field('footer_address_title','option') ;
								$footer_address = get_field('footer_address','option') ;
								if( $footer_address_title || $footer_address ) : ?>
									<ul class="adress-list">
				
										<?php if( $footer_address_title ) : ?>
											<li><?php echo $footer_address_title ?></li>
										<?php endif ; ?>
				
										<?php if( $footer_address ) : ?>
											<li>
												<address><?php echo $footer_address ; ?></address> 
											</li>
										<?php endif ; ?>
				
									</ul>
								<?php endif ; ?>
								
								<?php $footer_email = get_field('footer_email','option') ;
								$footer_phone = get_field('footer_phone','option') ;
								if( $footer_email || $footer_phone ) : ?>
									<ul class="contact-list">
				
										<?php if( $footer_email ) : ?>
											<li>
												<a href="mailto:<?php echo antispambot($footer_email) ; ?>"><?php echo antispambot($footer_email) ; ?></a>
											</li>
										<?php endif ; ?>
										
										<?php if( $footer_phone ) : ?>
											<li>
												<a href="tel:<?php echo clean_phone($footer_phone) ; ?>"><?php echo $footer_phone ; ?></a>
											</li>
										<?php endif ; ?>
				
									</ul>
								<?php endif ; ?>
				
								<?php if( have_rows('social_networks','option') ): ?>

									<ul class="icon-list">
				
										<?php while ( have_rows('social_networks','option') ) : the_row(); ?>
				
											<li>
												<a href="<?php echo get_sub_field('url') ; ?>" class="icon-<?php echo get_sub_field('icon') ; ?>" target="blank"></a>
											</li>
				
										<?php endwhile; ?>
				
									</ul>
				
								<?php endif; ?>
							</div>
							
							<?php if( has_nav_menu( 'primary' ) )
										wp_nav_menu( array(
											'container' => false,
											'theme_location' => 'primary',
											'menu_class'     => 'nav-tabs',
											'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
											'walker'         => new Custom_Walker_Nav_Menu
											)
										); ?>
						</div>
						
					</div>
				</nav>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					

				</div>
			</header>	

			<main role="main" id="main" class="main">
			
			