<?php
// Custom Post Types

function add_post_type() {

	 $labels_p = array(
        'name'                => __( 'Projets', 'netfirst' ),
        'singular_name'       => __( 'Projet', 'netfirst' ),
        'menu_name'           => __( 'Projet', 'netfirst' ),
        'parent_item_colon'   => __( 'Parent Projet', 'netfirst' ),
        'all_items'           => __( 'All Projets', 'netfirst' ),
        'view_item'           => __( 'View Projet', 'netfirst' ),
        'add_new_item'        => __( 'Add New Projet', 'netfirst' ),
        'add_new'             => __( 'Add New', 'netfirst' ),
        'edit_item'           => __( 'Edit Projet', 'netfirst' ),
        'update_item'         => __( 'Update Projet', 'netfirst' ),
        'search_items'        => __( 'Search Projet', 'netfirst' ),
        'not_found'           => __( 'Not Found', 'netfirst' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'netfirst' ),
    );
     
    $args_p = array(
        'label'               => __( 'projet', 'netfirst' ),
        'description'         => __( 'Quick link', 'netfirst' ),
        'labels'              => $labels_p,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 6,
        'menu_icon'           => 'dashicons-analytics',
        'can_export'          => true,
        'has_archive'         => true,
        //'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
		'rewrite' => array( 'slug' => 'projets' ),
    );
     
    register_post_type( 'projet', $args_p );

	$labels = array(
		'name' => _x( 'Filters', 'taxonomy general name', 'base' ),
		'singular_name' => _x( 'Filter', 'taxonomy singular name', 'base' ),
		'search_items' =>  __( 'Search Filters', 'base' ),
		'all_items' => __( 'All Filters', 'base' ),
		'parent_item' => __( 'Parent Filter', 'base' ),
		'parent_item_colon' => __( 'Parent Filter:', 'base' ),
		'edit_item' => __( 'Edit Filter', 'base' ), 
		'update_item' => __( 'Update Filter', 'base' ),
		'add_new_item' => __( 'Add New Filter', 'base' ),
		'new_item_name' => __( 'New Filter', 'base' ),
		'menu_name' => __( 'Filters', 'base' ),
	); 	

	register_taxonomy('projet-filter',array('projet'), array(
		'hierarchical' => false,
		'labels' => $labels,
		'show_ui' => true,
		'query_var' => true,
        'show_admin_column' => true,
		'rewrite' => array( 'slug' => 'projet-category' ),
	));
}

add_action( 'init', 'add_post_type', 0 );