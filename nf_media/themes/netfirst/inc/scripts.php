<?php
// Theme css & js

function base_scripts_styles() {
	$in_footer = true;
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	wp_deregister_script( 'comment-reply' );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply', get_template_directory_uri() . '/js/comment-reply.js', '', '', $in_footer );
	}

	// Loads JavaScript file with functionality specific.
	wp_enqueue_script( 'base-script', get_template_directory_uri() . '/js/jquery.main.js', array( 'jquery' ), '', $in_footer );
	wp_enqueue_script( 'animations', get_template_directory_uri() . '/js/animations.js', array( 'jquery' ), '', $in_footer );
	wp_enqueue_script( 'impl-script', get_template_directory_uri() . '/js/impl.js', array( 'jquery' ), '', $in_footer );


	wp_enqueue_script( 'scrollmagic', get_template_directory_uri() . '/js/ScrollMagic.min.js'  );

	wp_enqueue_script( 'timelineMax', get_template_directory_uri() . '/js/timelineMax.min.js'  );
	wp_enqueue_script( 'tweenMax', get_template_directory_uri() . '/js/tweenMax.min.js'  );


	wp_enqueue_script( 'gsap', get_template_directory_uri() . '/js/animation.gsap.js'  );




	// Loads our main stylesheet.
	wp_enqueue_style( 'base-style', get_stylesheet_uri(), array() );
	
	// Implementation stylesheet.
	wp_enqueue_style( 'base-theme', get_template_directory_uri() . '/theme.css', array() );	

	//animate
	wp_enqueue_style( 'animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css', array() );	
}
add_action( 'wp_enqueue_scripts', 'base_scripts_styles' );