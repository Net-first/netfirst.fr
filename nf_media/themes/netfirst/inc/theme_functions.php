<?php
// Theme functions

add_post_type_support( 'page', 'excerpt' );

add_filter( 'wpcf7_autop_or_not', '__return_false' );

function theme_change_query( $query ) {
    if (( $query->is_post_type_archive('projet') or $query->is_tax( 'projet-filter' ) ) && $query->is_main_query() ) {
        $query->set( 'posts_per_page', -1 );
    }
}
add_action( 'pre_get_posts', 'theme_change_query' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');



// SUPPRESION VER= DES FICHIERS JS & CSS

function remove_cssjs_ver( $src ) {
if( strpos( $src, "?ver=" ) )
$src = remove_query_arg( "ver", $src );
return $src;
}
add_filter("style_loader_src", "remove_cssjs_ver", 1000 );
add_filter( "script_loader_src", "remove_cssjs_ver", 1000 );