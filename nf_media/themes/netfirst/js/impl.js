jQuery(function() {
	initProjectFilter();
	initFormSuccesMessage() ;
});

function initProjectFilter(){
	var filterHolder = jQuery( '#filter' );
	var links = filterHolder.find( 'a' );

	links.on( 'click', function(e) {
		e.preventDefault();
		var link = jQuery(this);
		var value = link.data('filter');
		
		if ( link.hasClass( 'active' ) ) {
			link.removeClass( 'active' );
			value = '';
		}
		
		links.each( function( i, html ){
			jQuery( html ).removeClass( 'active' );
		} );
		
		if ( value ) {
			link.addClass( 'active' );
		}
	
		jQuery( '.container .items-holder a' ).filter(function() {
		    jQuery(this).toggle(jQuery(this).data( 'filter-value' ).toString().indexOf(value) > -1);
		});
	});
}

function initFormSuccesMessage(){

	var succesMessage = jQuery('.form-holder .form-info');
	var form = jQuery('.form-holder .wpcf7');
	var sectionTitle = jQuery('.contact-section .head-section') ;
	succesMessage.css('display','none');

	var wpcf7Elm = document.querySelector( '.wpcf7' );

	wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
		succesMessage.css('display','block');
		form.css('display','none');
		sectionTitle.css('display','none');
	}) ;

}
