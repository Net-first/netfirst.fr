<?php get_header(); ?>

<?php  

	$show_sidebar = get_field('show_sidebar') ;

?>

<section class="content-section">
	<div class="container">
		<div id="content">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_title( '<div class="title"><h1>', '</h1></div>' ); ?>
				<?php the_post_thumbnail( 'full' ); ?>
				<?php the_content(); ?>
				<?php edit_post_link( __( 'Edit', 'netfirst' ) ); ?>
			<?php endwhile; ?>
			<?php wp_link_pages(); ?>
			<?php comments_template(); ?>
		</div>
		<?php if( $show_sidebar ) : ?>
			<?php get_sidebar(); ?>
		<?php endif ; ?>
	</div>
</section>
<?php get_footer(); ?>