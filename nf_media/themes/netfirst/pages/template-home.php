<?php
/*
Template Name: Home Template
*/
get_header(); ?>

<?php  

	$vs_title = get_field('vs_title') ;
	$vs_video = get_field('vs_video') ;
	$vs_loader_image = get_field('vs_loader_image') ;

?>

<div class="line-1"></div>
<div class="line-2"></div>
<div class="line-3"></div>
<div class="line-4"></div>
<div class="line-5"></div>

<?php if( $vs_title || $vs_video ) : ?>
	<section class="hero-section">
		<div class="hero-info bg-video-holder" <?php if( $vs_loader_image ) : ?> style="background-image: url( <?php echo $vs_loader_image ?> );" <?php endif ; ?> >

			<?php if( $vs_video ) : ?>
				<video class="bg-video" width="640" height="360" loop autoplay muted playsinline>
				<source type="video/mp4" src="<?php echo $vs_video ; ?>" /></video>
			<?php endif ; ?>
			
			<?php if( $vs_title ) : ?>
				<h1><?php echo $vs_title ; ?></h1>
			<?php endif ; ?>

		</div>
	</section>
<?php endif ; ?>


<?php  

	$ms_title_bg = get_field('ms_title_bg') ;
	$ms_title = get_field('ms_title') ;
	$ms_text = get_field('ms_text') ;
	$ms_image = get_field('ms_image') ;

?>

<?php if( $ms_title_bg || $ms_title || $ms_text || $ms_image ) : ?>
	<section class="methodologie-section">

		<?php if( $ms_title_bg ) : ?>
			<div class="title-section gray">
				<p><?php echo $ms_title_bg ?></p>
			</div>
		<?php endif ; ?>
	
		<?php if( $ms_title || $ms_text ) : ?>
			<div class="section-info">

				<div class="container">

					<?php if( $ms_title ) : ?>
						<div class="head-holder">
							<h2><?php echo $ms_title ; ?></h2>
						</div>
					<?php endif ; ?>

					<div class="info-hodler">

						<?php if( $ms_text ) : ?>
							<div class="text-holder">
								<?php echo wpautop($ms_text) ; ?>
							</div>
						<?php endif ; ?>
						
						<?php if( $ms_image ) : ?>
							<div class="img-holder">
								<img src="<?php echo $ms_image['url'] ; ?>" alt="<?php echo $ms_image['alt'] ; ?>">
							</div>
						<?php endif ; ?>

					</div>
				</div>
			</div>
		<?php endif ; ?>

	</section>

<?php endif ; ?>


<?php  

	$ds_title_bg = get_field('ds_title_bg') ;
	$ds_title = get_field('ds_title') ;

?>

<?php if( $ds_title_bg || $ds_title || have_rows('ds_filters') ) : ?>

	<section class="digital-section">

		<?php if( $ds_title_bg ) : ?>
			<div class="title-section white">
				<p><?php echo $ds_title_bg ?></p>
			</div>
		<?php endif ; ?>

		<div class="container">
			<div class="section-info">
				
				<?php if( $ds_title ) : ?>
					<div class="head-holder">
						<h2><?php echo $ds_title ; ?></h2>
					</div>
				<?php endif ; ?>

				<?php if( have_rows('ds_filters') ): ?>

					<div class="info-items">

					    <?php while ( have_rows('ds_filters') ) : the_row(); ?>

					    	<?php  

					    		$term_id = get_sub_field('filter') ;
					    		$taxonomy = 'projet-filter' ;
					    		$term_info = get_term( $term_id, $taxonomy );
					    		$term_url = get_category_link( $term_id ) ;

					    	?>

					        <div class="item-holder">
								<h3><?php echo $term_info->name ; ?></h3>
								<div class="text-holder">
									<?php echo $term_info->description ; ?>
								</div>
								<div class="btn-holder">
									<a href="<?php echo $term_url ; ?>" class="btn"><?php _e('découvrir','netfirst') ?></a>
								</div>
							</div>

					    <?php endwhile; ?>

					</div>

				<?php endif; ?>

			</div>
		</div>
	</section>

<?php endif ; ?>


<?php  

	$is_title_bg = get_field('is_title_bg') ;
	$is_title = get_field('is_title') ;
	$is_image = get_field('is_image') ;
	$is_title_content = get_field('is_title_content') ;
	$is_content = get_field('is_content') ;
	$is_link = get_field('is_link') ;

?>

<?php if( $is_title_bg || $is_title || $is_page ) : ?>
	<section class="desidees-section">

		<?php if( $is_title_bg ) : ?>
			<div class="title-section gray">
				<p><?php echo $is_title_bg ?></p>
			</div>
		<?php endif ; ?>

		<div class="section-info">

			<?php if( $is_title ) : ?>
				<div class="head-holder">
					<h2><?php echo $is_title ; ?></h2>
				</div>
			<?php endif ; ?>
			
			<?php if( $is_image || $is_title_content || $is_content || $is_link ) : ?>
				<div class="info-wrap">

					<?php if( $is_image ) : ?>
						<div class="img-holder" style="background-image: url(<?php echo $is_image ?>);"></div>
					<?php endif ; ?>

					<div class="info-holder">
						<div class="holder-square">

							<?php if( $is_title_content ) : ?>
								<h3><?php echo $is_title_content ; ?></h3>
							<?php endif ; ?>

							<?php if( $is_content ) : ?>
								<div class="text-holder">
									<?php echo $is_content ; ?>
								</div>
							<?php endif ; ?>

							<?php if( $is_link ) : ?>
								<div class="btn-holder">
									<a href="<?php echo $is_link['url']; ?>" class="btn"><?php echo $is_link['title']; ?></a>
								</div>
							<?php endif ; ?>

						</div>
					</div>
				</div>
			<?php endif ; ?>

		</div>
	</section>
<?php endif ; ?>


<?php  

	$ps_title_bg = get_field('ps_title_bg') ;
	$ps_title = get_field('ps_title') ;
	$ps_projets_number = get_field('ps_projets_number') ;

?>

<?php if( $ps_title_bg || $ps_title || $ps_projets_number ) : ?>

	<section class="desprojets-section">

		<?php if( $ps_title_bg ) : ?>
			<div class="title-section white">
				<p><?php echo $ps_title_bg ?></p>
			</div>
		<?php endif ; ?>
	
		<div class="section-info">

			<?php if( $ps_title ) : ?>
				<div class="head-holder">
					<h2><a href="<?php echo get_post_type_archive_link('projet') ; ?>"><?php echo $ps_title ; ?></a></h2>
				</div>
			<?php endif ; ?>

			<?php

				if( $ps_projets_number ){

					$posts_p_arr = array(
						'post_type' => 'projet',
						'posts_per_page' => $ps_projets_number
					);

					$posts_p_query = new WP_Query( $posts_p_arr ) ; 
					
				}


			?>
		
			<?php if( $posts_p_query->have_posts() ) : ?>
				<div class="slick-slider-frame">

					<?php while( $posts_p_query->have_posts() ) : $posts_p_query->the_post(); ?>

						<?php  

							$screen_image = get_field('screen_image') ;
							$background_image = get_field('background_image') ;
							$subtitle = get_field('subtitle') ;

						?>

						<div class="slider-item">
							<div class="slider-text">
								<h1><?php the_title() ; ?></h1>

								<?php if( $subtitle ) : ?>
									<p><?php echo $subtitle ; ?></p>
								<?php endif ; ?>

								<a href="<?php the_permalink() ; ?>" class="btn"><?php _e('voir le projet','netfirst') ; ?></a>
							</div>

							<?php if( $screen_image ) : ?>
								<div class="slider-img">
									<div class="overlay"></div>
									<img src="<?php echo $screen_image['url'] ; ?>" alt="<?php echo $screen_image['alt'] ; ?>">
								</div>
							<?php endif ; ?>
							
							<?php if( $background_image ) : ?>
								<div class="slider-bg">		
									<img src="<?php echo $background_image['url'] ; ?>" alt="<?php echo $background_image['alt'] ; ?>">
								</div>
							<?php endif ; ?>

						</div>

					<?php endwhile ; ?>
					
				</div>
				<?php wp_reset_postdata(); ?>
			<?php endif ; ?>

		</div>
	</section>
<?php endif ; ?>


<?php  

	$cs_title_bg = get_field('cs_title_bg') ;
	$cs_title = get_field('cs_title') ;

?>

<?php if( $cs_title_bg || $cs_title || have_rows('clients') ) : ?>
	<section class="references-section">

		<?php if( $cs_title_bg ) : ?>
			<div class="title-section gray">
				<p><?php echo $cs_title_bg ?></p>
			</div>
		<?php endif ; ?>

		<div class="section-info">

			<?php if( $cs_title ) : ?>
				<div class="head-holder">
					<h2><?php echo $cs_title ; ?></h2>
				</div>
			<?php endif ; ?>
			
			<?php if( have_rows('clients') ) : ?>
				<div class="container">
					<ul class="references-list">

						<?php while ( have_rows('clients') ) : the_row(); ?>

							<?php  

								$image_client = get_sub_field('image') ;

							?>

							<li class="list-item">
								<img src="<?php echo $image_client['url'] ; ?>" alt="<?php echo $image_client['alt'] ; ?>">
							</li>

						<?php endwhile ; ?>

					</ul>
				</div>
			<?php endif ; ?>

		</div>
	</section>
<?php endif ; ?>

<?php get_template_part( 'blocks/before_footer' ) ?>

<?php get_footer(); ?>