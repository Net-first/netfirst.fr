<?php
/*
Template Name: lagence Template
*/
get_header(); the_post() ?>
<a href="#" class="scroll-down"><img src="<?php echo get_template_directory_uri() ?>/fonts/scroll.svg" alt="image description"><span><?php _e( 'Devouvrir', 'netfirst' ) ?></span></a>

<div class="line-1"></div>
<div class="line-2"></div>
<div class="line-3"></div>
<div class="line-4"></div>
<div class="line-5"></div>

<section class="lagence-section" style="background-image: url(<?php has_post_thumbnail() ? the_post_thumbnail_url( 'full' ) : '' ?>);">
    <h1><?php the_title() ?></h1>
    <div class="text-holder">
        <?php the_content(); ?>
        <?php edit_post_link( __( 'Edit', 'netfirst' ) ); ?>
        <?php wp_link_pages(); ?>
    </div>
</section>

<?php $title = get_field( 'lagence_template_info_title' );
if( have_rows('lagence_template_info') ): ?>
    <section class="info-section">
        <?php if( $title ) : ?>
            <div class="head-holder">
                <h2><?php echo $title ?></h2>
            </div>
        <?php endif ?>
    
        <?php while( have_rows('lagence_template_info') ): the_row(); 
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $text = get_sub_field('text');
            $_i = acf_get_loop( 'active', 'i' );
            ?>
            <div class="item-holder <?php echo $_i % 2 ? 'right' : '' ?>">
                <div class="img-holder">
                    <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
                </div>
                <div class="text-holder">
                    <div class="text">
                        <?php if( $title ) : ?>
                            <h3><?php echo $title ?></h3>
                        <?php endif ?>
                        <?php echo $text ?>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>
<?php get_template_part( 'blocks/before_footer' ) ?>
<?php get_footer(); ?>