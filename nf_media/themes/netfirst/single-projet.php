<?php get_header(); the_post() ?>

<a href="#" class="scroll-down"><img src="<?php echo get_template_directory_uri() ?>/fonts/scroll.svg" alt="image description"><span><?php _e( 'Découvrir', 'netfirst' ) ?></span></a>
<div class="line-1"></div>
<div class="line-2"></div>
<div class="line-3"></div>
<div class="line-4"></div>
<div class="line-5"></div>

<section class="blow-section">

	<?php $pre_header = get_field('pre_header') ; ?>

	<?php if( $pre_header ) : ?>
		<h3><?php echo $pre_header ; ?></h3>
	<?php endif ; ?>

	<div class="secondhead-holder">
		<h1><?php the_title() ?></h1>
	</div>
	<div class="container">
		<div class="text-holder">
			<?php the_content(); ?>
			<?php edit_post_link( __( 'Edit', 'netfirst' ) ); ?>
			<?php wp_link_pages(); ?>
		</div>
		<div class="icon-holder">
			<?php $client_info = get_field( 'client_info' );
			$c1t = isset( $client_info['column_1_top'] ) ? $client_info['column_1_top'] : '';
			$c1b = isset( $client_info['column_1_bottom'] ) ? $client_info['column_1_bottom'] : '';
			$c2t = isset( $client_info['column_2_top'] ) ? $client_info['column_2_top'] : '';
			$c2b = isset( $client_info['column_2_bottom'] ) ? $client_info['column_2_bottom'] : '';
			if( $c1t or $c1b or $c2t or $c2b ) : ?>
				<ul class="client">
					<?php if( $c1t or $c1b ) : ?>
						<li><span><?php echo $c1t ?></span><span><?php echo $c1b ?></span></li>
					<?php endif ?>
					<?php if( $c2t or $c2b ) : ?>
						<li><span><?php echo $c2t ?></span><span><?php echo $c2b ?></span></li>
					<?php endif ?>
				</ul>
			<?php endif ?>
			<?php /*$post_terms = wp_get_post_terms( get_the_ID(), 'projet-filter' );
			if( $post_terms ) : ?>
				<ul class="blow-icon">
					<?php foreach( $post_terms as $post_term ) :
						$icon = get_field( 'project_filter_icon', $post_term );
						if( $icon ) : ?>
							<li><img src="<?php echo $icon['url'] ?>" alt="<?php echo $icon['alt'] ?>"></li>
						<?php endif ?>
					<?php endforeach ?>		
				</ul>
			<?php endif*/ ?>
		</div>
	</div>
</section>
<section class="bigslider-section">
	<?php
	$left_image = get_field( 'left_image' );
	$right_image = get_field( 'right_image' );

	if( $left_image or $right_image or $bottom_image ) :
	?>
	<div class="item">
		<div class="slider-img-holder">
			<?php if( $right_image ) : ?>
				<div class="slider-img">
					<img src="<?php echo $right_image['url'] ?>" alt="<?php echo $right_image['alt'] ?>">
				</div>
			<?php endif ?>
			<?php if( $left_image ) : ?>
				<div class="slider-bg">
					<img src="<?php echo $left_image['url'] ?>" alt="<?php echo $left_image['alt'] ?>">
				</div>
			<?php endif ?>
		</div>

		<?php if( have_rows('bottom_images') ): ?>

			<?php while ( have_rows('bottom_images') ) : the_row(); ?>

				<?php $image = get_sub_field( 'image' ); ?>

				<?php if( $image ) : ?>
					<div class="product-holer">
						<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
					</div>
				<?php endif ?>

			<?php endwhile ; ?>

		<?php endif ; ?>

	</div>
	<?php
	endif;
	?>
	<div class="btn-holder">
		<?php $prev = get_previous_post_link( '%link', '<span>' . __( 'Projet précédant', 'netfirst' ) . '</span><span>%title</span>' ) ?>
		<?php $next = get_next_post_link( '%link', '<span>' . __( 'Projet suivant', 'netfirst' ) . '</span><span>%title</span>' ) ?>
		<?php if( $prev || $next ) : ?>
			<div class="navigation-single">
				<?php if( $prev ) : ?><div class="prev"><?php echo $prev ?></div><?php endif ?>
				<?php if( $next ) : ?><div class="next"><?php echo $next ?></div><?php endif ?>
			</div>
		<?php endif ?>
	</div>
</section>
<?php get_template_part( 'blocks/before_footer' ) ?>
<?php get_footer(); ?>