<?php get_header(); ?>
<section class="content-section">
	<div class="container">
		<div id="content">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'blocks/content', get_post_type() ); ?>
			<?php endwhile; ?>
		</div>

		<?php get_template_part( 'inc/social-share' ); ?>


		
	</div>
</section>
<?php get_footer(); ?>