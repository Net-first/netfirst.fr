<?php get_header(); ?>
<?php $term = get_queried_object(); ?>

<a href="#" class="scroll-down"><img src="<?php echo get_template_directory_uri() ?>/fonts/scroll.svg" alt="image description"><span><?php _e( 'Devouvrir', 'netfirst' ) ?></span></a>

<div class="line-1"></div>
<div class="line-2"></div>
<div class="line-3"></div>
<div class="line-4"></div>
<div class="line-5"></div>
<section class="digitalpage-section">
	<div class="secondhead-holder">
		<h1><?php echo $term->name ?></h1>
	</div>
	<div class="container">
		<div class="info-holder">
			<div class="text-holder">
				<?php 
				$icon = get_field( 'project_filter_icon_taxonomy', $term );
				$image = get_field( 'project_filter_image', $term );
				$description = get_field( 'project_filter_description', $term );
				
				if( $icon ) :
				?>
					<div class="icon-png img-holder" style="background-image: url(<?php echo $icon['url'] ?>);"></div>
				<?php
				endif;
				
				echo $description;
				?>
			</div>
		</div>
	</div>
	<?php if( $image ) : ?>
		<div class="img-holder">
			<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
		</div>
	<?php endif ?>
</section>

<?php
$title_1_left = get_field( 'project_filter_title_1_left', $term );
$text_1_left = get_field( 'project_filter_text_1_left', $term );
$image_phone = get_field( 'project_filter_image_phone', $term );
$title_2_right = get_field( 'project_filter_title_2_right', $term );
$text_2_right = get_field( 'project_filter_text_2_right', $term );
$image_laptop = get_field( 'project_filter_image_laptop', $term );
$title_3_left = get_field( 'project_filter_title_3_left', $term );
$text_3_left = get_field( 'project_filter_text_3_left', $term );

if( $title_1_left or $text_1_left or $image_phone or $title_2_right or $text_2_right or $image_laptop or $title_3_left or $text_3_left ) :
?>
	<section class="functionalinfo-section">
		<div class="item-holder">
			<div class="text-holder top-left">
				<div class="text">
					<?php if( $title_1_left ) : ?>
						<h3><?php echo $title_1_left ?></h3>
					<?php endif ?>
					<?php echo $text_1_left ?>
				</div>
			</div>
		</div>
		<div class="bg-ipone">
			<?php if( $image_phone ) : ?>
				<img src="<?php echo $image_phone['url'] ?>" alt="<?php echo $image_phone['alt'] ?>">
			<?php endif ?>
		</div>
		<div class="item-holder right">
			<div class="text-holder bottom-right">
				<div class="text">
					<?php if( $title_2_right ) : ?>
						<h3><?php echo $title_2_right ?></h3>
					<?php endif ?>
					<?php echo $text_2_right ?>
				</div>
			</div>
		</div>
		<div class="bg-mac">
			<?php if( $image_laptop ) : ?>
				<img src="<?php echo $image_laptop['url'] ?>" alt="<?php echo $image_laptop['alt'] ?>">
			<?php endif ?>
		</div>
		<div class="item-holder">
			<div class="text-holder bottom-left">
				<div class="text">
					<?php if( $title_3_left ) : ?>
						<h3><?php echo $title_3_left ?></h3>
					<?php endif ?>
					<?php echo $text_3_left ?>
				</div>
			</div>
		</div>
	</section>
<?php
endif;
?>
<?php if ( have_posts() ) : ?>
	<section class="desprojets-section">
		<div class="section-info">
			<?php
			$title = get_field( 'project_filter_title_before_projects', $term );
			if( $title ) : ?>
				<div class="head-holder">
					<h2><?php echo $title ?></h2>
				</div>
			<?php endif ?>
			<div class="slick-slider-frame">
				<?php while ( have_posts() ) : the_post(); ?>
					<div class="slider-item">
						<div class="slider-text">
							<h1><?php the_title() ?></h1>
							<?php echo wpautop( get_field( 'subtitle' ) ) ?>
							<a href="<?php the_permalink() ?>" class="btn"><?php _e( 'voir le projet', 'netfirst' ) ?></a>
						</div>
						<?php
						$screen_image = get_field( 'screen_image' );
						$background_image = get_field( 'background_image' );
						if( $screen_image ) : ?>
							<div class="slider-img">
							<div class="overlay"></div>
								<img src="<?php echo $screen_image['url'] ?>" alt="<?php echo $screen_image['alt'] ?>">
							</div>
						<?php endif ?>
						<?php if( $background_image ) : ?>
							<div class="slider-bg">
								<img src="<?php echo $background_image['url'] ?>" alt="<?php echo $background_image['alt'] ?>">
							</div>
						<?php endif ?>
					</div>
				<?php endwhile; ?>
			</div>
			<?php get_template_part( 'blocks/pager' ); ?>
		</div>
	</section>
<?php endif; ?>
<?php get_template_part( 'blocks/before_footer' ) ?>
<?php get_footer(); ?>